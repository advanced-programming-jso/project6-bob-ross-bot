#include "shape.h"

class Rectangle : public Shape {
public:
	Rectangle(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};