#include "shape.h"

class Ellipse : public Shape {
public:
	Ellipse(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};