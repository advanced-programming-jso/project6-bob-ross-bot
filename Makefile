#declare the variable
CC=g++

CFLAGS=-c

all: core

core: circle.o rectangle.o script.o line.o polygon.o ellipse.o picture.o shape.o group.o layer.o bob.o command.o workspace.o main.o
	  	$(CC) circle.o rectangle.o script.o line.o polygon.o ellipse.o picture.o shape.o group.o layer.o bob.o command.o workspace.o main.o -o output

main.o: main.cpp
		$(CC) $(CFLAGS) main.cpp

workspace.o: workspace.cpp workspace.h
		$(CC) $(CFLAGS) workspace.cpp

command.o: command.cpp command.h
		$(CC) $(CFLAGS) command.cpp

bob.o: bob.cpp bob.h 
		$(CC) $(CFLAGS) bob.cpp

layer.o: layer.cpp layer.h 
		$(CC) $(CFLAGS) layer.cpp

group.o: group.cpp group.h 
		$(CC) $(CFLAGS) group.cpp

shape.o: shape.cpp shape.h
		$(CC) $(CFLAGS) shape.cpp

circle.o:  circle.cpp circle.h 
		$(CC) $(CFLAGS) circle.cpp

rectangle.o: rectangle.cpp rectangle.h 
		$(CC) $(CFLAGS) rectangle.cpp

script.o: script.cpp script.h 
		$(CC) $(CFLAGS) script.cpp

line.o: line.cpp line.h 
		$(CC) $(CFLAGS) line.cpp

polygon.o: polygon.cpp polygon.h 
		$(CC) $(CFLAGS) polygon.cpp

ellipse.o: ellipse.cpp ellipse.h 
		$(CC) $(CFLAGS) ellipse.cpp

picture.o: picture.cpp picture.h 
		$(CC) $(CFLAGS) picture.cpp

clean:
		rm -rf *o core