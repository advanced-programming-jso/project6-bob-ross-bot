#include "shape.h"

class Script : public Shape {
public:
	Script(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};