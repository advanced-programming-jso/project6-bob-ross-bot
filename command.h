#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>

#ifndef COMMAND_H
#define COMMAND_H

	class Command {
	public:
		Command() {}
		Command(std::string s);
		void set_command(std::string s);
		std::vector<std::string> get_value(std::string s);
		std::string get_command_type();
		std::string get_command();
		void add_name(std::string name);

	private:
		std::string command;
		std::vector<std::string> splited_command;
	};

	std::vector<std::string> parser(std::string s);
	void replace_coma (std::string& s);
	void replace_bracket (std::string& s);
	void replace_felesh (std::string& s);
	void value_parser(std::string s, std::vector<std::string>& v);
#endif 