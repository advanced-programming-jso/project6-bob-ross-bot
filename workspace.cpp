#include "workspace.h"

using namespace std;

Workspace::Workspace() {

};

void Workspace::save_commands(Command* c) {
	ofstream out((c->get_value("save"))[0].c_str());
	for(int i = 0; i < commands.size(); i++){
    	out << commands[i]->get_command() << endl;
	}
    out.close();
};

void read_file(string name, Workspace& w) {
	string s;
	ifstream infile;
	infile.open (name.c_str());
	getline(infile, s);
	Command* c = new Command(s);
	Bob b(c);
	w.bob = &b;
	w.commands.push_back(c);
    while(getline(infile, s)) {
	   	Command* c = new Command(s);
	   	bool repeated = false;
	   	string cm_type = c->get_command_type();
	   	if(cm_type == "save")
	   		w.save_commands(c);
	   	else if(cm_type == "draw") {
	   		if((c->get_value("draw")).size() < 2)
	   			w.set_new_name(c);
	   		else 
	   			repeated = w.is_name_repeated(c->get_value("draw")[1]);
	   	} else if(cm_type == "group") {
	   		if(c->get_value("group").size() < 1)
	   			w.set_new_name(c);
	   		else
	   			repeated = w.is_name_repeated(c->get_value("group")[0]);
	   	}
	   	if(!repeated){
	   		w.commands.push_back(c);
	   		w.bob->set_command(c);
	   	}
    }
	infile.close();
	read_terminal(w);
};

bool Workspace::is_name_repeated(std::string s) {
	for(int i = 1; i < commands.size(); i++) {
		if((commands[i]->get_value("draw")).size() > 0 && (commands[i]->get_value("draw"))[1] == s)
			return true;
		if((commands[i]->get_value("group")).size() > 0 && (commands[i]->get_value("group"))[0] == s)
			return true;
	}
	return false;
};

void Workspace::set_new_name(Command* c) {
	int i = 0;
	bool is_repeated = true;
	string new_name;
	while(true) {
		if(c->get_command_type() == "draw") {
			new_name = (c->get_value("draw"))[0] + int_to_str(i);
			if(is_name_repeated(new_name))
				i = i + 1;
			else {
				c->add_name(new_name);
				break;
			}	
		} else if (c->get_command_type() == "group") {
			new_name = "group" + int_to_str(i);
			if(is_name_repeated(new_name))
				i = i + 1;
			else {
				c->add_name(new_name);
				break;
			}	
		}
	}
};

void read_terminal(Workspace& w) {
	string s;
	while(getline(cin, s)) {
		Command* c = new Command(s);
		bool repeated = false;
		string cm_type = c->get_command_type();
	   	if(cm_type == "save")
	   		w.save_commands(c);
	   	else if(cm_type == "draw") {
	   		if((c->get_value("draw")).size() < 2)
	   			w.set_new_name(c);
	   		else 
	   			repeated = w.is_name_repeated(c->get_value("draw")[1]);
	   	} else if(cm_type == "group") {
	   		if(c->get_value("group").size() < 1)
	   			w.set_new_name(c);
	   		else
	   			repeated = w.is_name_repeated(c->get_value("group")[0]);
	   	}
	   	if(!repeated){
	   		w.commands.push_back(c);
	   		w.bob->set_command(c);
	   	}
	};
};

istream& operator>>(istream& in, Workspace& w) { 
	string s;
	getline(cin, s);
	if(s.substr(0, s.find(' ')) == "init") {
		Command* c = new Command(s);
		Bob b(c);
		w.bob = &b;
		w.commands.push_back(c);
		read_terminal(w);
	} else if (s.substr(0, s.find(' ')) == "import")
		read_file(s.substr(s.find(' ') + 1), w);
	else
		cin >> w;
	return in; 
};