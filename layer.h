#include "command.h"
#include <string>

#ifndef LAYER_H
#define LAYER_H

class Layer {
public:
	virtual void shape_builder(Command* cm) = 0;
    virtual std::string export_svg() = 0;
    virtual void move_layer(Command* c) = 0;
    virtual std::string get_name() = 0;
    virtual void ungroup(Command* c) = 0;
    virtual std::vector<Layer*> get_layers() = 0;
    virtual void move_obj(Command* move, Command* c) = 0;
    virtual Command* get_command() = 0;
    virtual void find_moving_obj(Command* c, Command* temp) = 0;
    virtual void list(Command* c) = 0;
private:

};

#endif