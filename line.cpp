#include "line.h"

using namespace std;

Line::Line(Command* c) : Shape(c) {
	command = c;
};

string Line::export_svg() {
	string svg = "<line ";
	if((command->get_value("points")).size() > 0)
		svg += "x1=\"" + (command->get_value("points"))[0] + "\" " + "y1=\"" + (command->get_value("points"))[1] + "\" "
				+ "x2=\"" + (command->get_value("points"))[2] + "\" " + "y2=\"" + (command->get_value("points"))[3] + "\" ";
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Line::make_shape() {

};