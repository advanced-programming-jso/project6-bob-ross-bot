#include "script.h"

using namespace std;

Script::Script(Command* c) : Shape(c) {
	command = c;
};

string Script::export_svg() {
	string svg = "<text ";
	if((command->get_value("position")).size() > 0){
		svg += "x=\"" + (command->get_value("position"))[0] + "\" " + "y=\"" + (command->get_value("position"))[1] + "\" ";
		svg += styles(command);
		svg += ">";
	}
	if((command->get_value("text")).size() > 0)
		svg += (command->get_value("text"))[0];
	svg += "</text>\n";
	return svg;
};

void Script::make_shape() {

};