#include "circle.h"

using namespace std;

Circle::Circle(Command* c) : Shape(c) {
	command = c;
};

string Circle::export_svg() {
	string svg = "<circle ";
	if((command->get_value("center")).size() > 0)
		svg += "cx=\"" + (command->get_value("center"))[0] + "\" " + "cy=\"" + (command->get_value("center"))[1] + "\" ";
	if((command->get_value("radius")).size() > 0)
		svg += "r=\"" + (command->get_value("radius"))[0] + "\" ";
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Circle::make_shape() {

};

/*
void Circle::print() {
	cout << command->get_command_type() << endl;
};
*/