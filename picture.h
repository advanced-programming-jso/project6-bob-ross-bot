#include "shape.h"

class Picture : public Shape {
public:
	Picture(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};