#include "group.h"

using namespace std;

void Group::shape_builder(Command* cm) {
	string cm_name = (cm->get_value("draw"))[0];
	if(cm_name == "circle"){
		Circle* c = new Circle(cm);
		layers.push_back(c);
	} else if(cm_name == "rectangle") {
		Rectangle* r = new Rectangle(cm);
		layers.push_back(r);
	} else if(cm_name == "line") {
		Line* l = new Line(cm);
		layers.push_back(l);
	} else if(cm_name == "script") {
		Script* s = new Script(cm);
		layers.push_back(s);
	} else if(cm_name == "polygon") {
		Polygon* p = new Polygon(cm);
		layers.push_back(p);
	} else if(cm_name == "ellipse") {
		Ellipse* e = new Ellipse(cm);
		layers.push_back(e);
	} else if(cm_name == "picture") {
		Picture* p = new Picture(cm);
		layers.push_back(p);
	}
	// for(int i = 0; i < layers.size(); i++) {
	// 	layers[i]->print();
	// }
};

void Group::group_builder(Command* cm) {
	Group* g = new Group(cm);
	layers.push_back(g);
};

string Group::export_svg() {
	//cout << layers.size() << endl;
	string temp = "";
	if(command->get_value("group")[0] != "root"){
		temp = "<g ";
		temp += styles(command);
		temp += ">\n";
	}
	for(int i = 0; i < layers.size(); i++) {
	 	temp += layers[i]->export_svg();
	}
	if(command->get_value("group")[0] != "root")
		temp += "</g>\n";
	return temp;
};

void Group::print() {
	cout << ":::::" << endl;
	cout << layers.size();
	for(int i = 0; i < layers.size(); i++) {
		layers[i]->export_svg();
	}
};

void Group::swap(int i1, int i2) {
	Layer* temp = layers[i1];
	layers[i1] = layers[i2];
	layers[i2] = temp;
};

void Group::move_layer(Command* c) {
	for(int i = 0; i < layers.size(); i++) {
		if(layers[i]->get_name() == c->get_value("bring")[1]){
			if(c->get_value("bring")[0] == "up" && i != layers.size()-1){
				swap(i, i+1);
				break;
			} else if(c->get_value("bring")[0] == "down" && i != 0) {
				swap(i, i-1);
				break;
			}
		}
		layers[i]->move_layer(c);
	}
	return;
};

void Group::ungroup(Command* c) {
	for (int i = 0; i < layers.size(); i++) {
		if(layers[i]->get_name() == c->get_value("ungroup")[0]) {
			vector<Layer*> ungrouped = layers[i]->get_layers();
			for(int j = 0; j < ungrouped.size(); j++) {
				layers.insert(layers.begin() + i + j, ungrouped[j]);
			}
			layers.erase(layers.begin() + i);
			break;
		}
		layers[i]->ungroup(c);
	}
};


void Group::move_obj(Command* move, Command* c) {
	string gp_name;
	gp_name = move->get_value("move")[2];
	for (int i = 0; i < layers.size(); i++) {
		if(layers[i]->get_name() == move->get_value("move")[2]) {
			layers[i]->shape_builder(c);
			break;
		}
		layers[i]->move_obj(move, c);
	}
};

void Group::find_moving_obj(Command* c, Command* temp) {
	for (int i = 0; i < layers.size(); i++) {
		if(layers[i]->get_name() == c->get_value("move")[0]) {
			Layer* temp2 = layers[i];
			layers.erase(layers.begin() + i);
			temp->set_command((temp2->get_command())->get_command());
			break;
		}
		layers[i]->find_moving_obj(c, temp);
	}
};

void Group::list(Command* c) {
	for (int i = 0; i < layers.size(); i++) {
		if(layers[i]->get_name() == c->get_value("list")[0]) {
			vector<Layer*> list = layers[i]->get_layers();
			for(int j = 0; j < list.size(); j++) {
				cout << list[j]->get_name() << endl;
			}
			break;
		}
		layers[i]->list(c);
	}
};

/*
void Group::print() {
	cout << command->get_command_type() << endl;
};
*/