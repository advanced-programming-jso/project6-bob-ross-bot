#include "command.h"
#include "layer.h"

#ifndef SHAPE_H
#define SHAPE_H

class Shape : public Layer {
public:
	Shape(Command* c) : command(c) {}
	virtual void make_shape() = 0;
	std::string get_name() { return command->get_value("draw")[1]; }
	void move_layer(Command* c) {}
	void ungroup(Command* c) {}
    std::vector<Layer*> get_layers() {}
    void move_obj(Command* move, Command* c) {}
    Command* get_command() { return command; }
    void shape_builder(Command* cm) {}
    void find_moving_obj(Command* c, Command* temp) {}
    void list(Command* c) {}
protected:
	Command* command;
};

std::string int_to_str(int num);
std::string styles(Command* c);

#endif