#include "polygon.h"

using namespace std;

Polygon::Polygon(Command* c) : Shape(c) {
	command = c;
};

string Polygon::export_svg() {
	string svg = "<polygon ";
	if((command->get_value("points")).size() > 0){
		svg += "points=\"";
		for(int i = 0; i < (command->get_value("points")).size(); i+=2) {
			svg += (command->get_value("points"))[i] + "," + (command->get_value("points"))[i+1] + " ";
		}
		svg +="\" ";
	}
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Polygon::make_shape() {

};