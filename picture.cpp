#include "picture.h"

using namespace std;

Picture::Picture(Command* c) : Shape(c) {
	command = c;
};

string Picture::export_svg() {
	string svg = "<image ";
	if((command->get_value("address")).size() > 0)
		svg += "xlink:href=\"" + (command->get_value("address"))[0] + "\" ";
	if((command->get_value("points")).size() > 0)
		svg += "x=\"" + (command->get_value("points"))[0] + "\" " + "y=\"" + (command->get_value("points"))[1] + "\" "
				+ "width=\"" + int_to_str(atoi((command->get_value("points"))[2].c_str()) - atoi((command->get_value("points"))[0].c_str()))
				+ "\" " + "height=\"" + int_to_str(atoi((command->get_value("points"))[3].c_str()) - atoi((command->get_value("points"))[1].c_str())) + "\" ";
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Picture::make_shape() {

};