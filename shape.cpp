#include "shape.h"

using namespace std;

string int_to_str(int num) {
    stringstream ss;
    ss << num;
    return ss.str();
}

string styles(Command* c) {
	string styles = "style=\"";
	if(c->get_value("font").size() > 0)
		styles += "font-family:" + c->get_value("font")[0] + ";font-size:" + c->get_value("font")[1] + ";";
	if(c->get_value("color").size() > 0)
		styles += "fill:" + c->get_value("color")[0] + ";";
	if(c->get_value("border").size() > 0)
		styles += "stroke:" + c->get_value("border")[0] + ";stroke-width:" + c->get_value("border")[1] + ";";
	if(c->get_value("opacity").size() > 0)
		styles += "opacity:" + c->get_value("opacity")[0] + ";";
	if(c->get_value("rotate").size() > 0)
		styles += "transform:rotate(" + c->get_value("rotate")[0] + ");";
	styles = styles.substr(0, styles.length() - 1);
	if(styles == "style=")
		return "";
	return styles + "\"";
}