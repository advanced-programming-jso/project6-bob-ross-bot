#include "command.h"
#include "bob.h"
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

class Workspace {
public:
	Workspace();
	void save_commands(Command* c);
	bool is_name_repeated(std::string s);
	void set_new_name(Command* c);
	friend std::istream& operator>>(std::istream& in, Workspace& w);
	friend void read_file(std::string name, Workspace& w);
	friend void read_terminal(Workspace& w);
private:
	std::vector<Command*> commands;
	Bob* bob;
};