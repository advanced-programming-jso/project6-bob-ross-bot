#include "shape.h"

class Circle : public Shape {
public:
	Circle(Command* c);
	void make_shape();
	std::string export_svg();
	//void print();
private:
	Command* command;
};