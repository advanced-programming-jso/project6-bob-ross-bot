#include "ellipse.h"

using namespace std;

Ellipse::Ellipse(Command* c) : Shape(c) {
	command = c;
};

string Ellipse::export_svg() {
	string svg = "<ellipse ";
	if((command->get_value("center")).size() > 0)
		svg += "cx=\"" + (command->get_value("center"))[0] + "\" " + "cy=\"" + (command->get_value("center"))[1] + "\" ";
	if((command->get_value("radius")).size() > 0)
		svg += "rx=\"" + (command->get_value("radius"))[0] + "\" " + "ry=\"" + (command->get_value("radius"))[1] + "\" ";
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Ellipse::make_shape() {

};