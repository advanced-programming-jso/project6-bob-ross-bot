#include "circle.h"
#include "rectangle.h"
#include "line.h"
#include "script.h"
#include "polygon.h"
#include "ellipse.h"
#include "picture.h"
#include "layer.h"
#include <vector>

#ifndef GROUP_H
#define GROUP_H

class Group : public Layer{
public:
	Group(Command* c) : command(c) {}
	void shape_builder(Command* cm);
	void group_builder(Command* cm);
	std::string export_svg();
	void move_layer(Command* c);
	void swap(int l1, int l2);
	void ungroup(Command* c);
    std::vector<Layer*> get_layers() { return layers; }
	void move_obj(Command* move, Command* c);
    Command* get_command() { return command; }
	void print();
	std::string get_name() { return command->get_value("group")[0]; }
	void find_moving_obj(Command* c, Command* temp);
	void list(Command* c);
private:
	std::vector<Layer*> layers;
	Command* command;
};

#endif