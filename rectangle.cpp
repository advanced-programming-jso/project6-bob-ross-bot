#include "rectangle.h"

using namespace std;

Rectangle::Rectangle(Command* c) : Shape(c) {
	command = c;
};

string Rectangle::export_svg() {
	string svg = "<rect ";
	if((command->get_value("points")).size() > 0)
		svg += "x=\"" + (command->get_value("points"))[0] + "\" " + "y=\"" + (command->get_value("points"))[1] + "\" "
				+ "width=\"" + int_to_str(atoi((command->get_value("points"))[2].c_str()) - atoi((command->get_value("points"))[0].c_str()))
				+ "\" " + "height=\"" + int_to_str(atoi((command->get_value("points"))[3].c_str()) - atoi((command->get_value("points"))[1].c_str())) + "\" ";
	svg += styles(command);
	svg += "/>\n";
	return svg;
};

void Rectangle::make_shape() {

};