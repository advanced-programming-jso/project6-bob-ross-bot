#include "shape.h"

class Line : public Shape {
public:
	Line(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};