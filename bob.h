#include "command.h"
#include "group.h"

class Bob {
public:
	Bob(Command* c);	
	void set_command(Command* c);
	void svg_out();
private:
	int width, height;
	Command* command;
	Group* root;
};