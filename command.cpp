#include "command.h"
#include <cstdlib>

using namespace std;

Command::Command(string s) {
	set_command(s);
};

void Command::set_command(std::string s) {
	command = s;
	splited_command = parser(s);	
}

string Command::get_command_type() {
	return splited_command[0];
};

string Command::get_command() {
	return command;
};

void Command::add_name(string name) {
	for(int i = 0; i < splited_command.size(); i++) {
		if(splited_command[i] == "||") {
			splited_command.insert(splited_command.begin()+i, name);
			break;
		}
	}

	//for(int i = 0; i < splited_command.size(); i++)
	//	cout << splited_command[i] << endl;
};

vector<string> Command::get_value(string s) {
	vector<string> temp;
	for(int i = 0; i < splited_command.size(); i++) {
		if (splited_command[i] == s) {
			for(int j = i + 1; j < splited_command.size(); j++) {
				if(splited_command[j] != "||")
					temp.push_back(splited_command[j]);
				else
					break;
			}
		}
	}
	//for(int i = 0; i < temp.size(); i++)
	//	cout << temp[i] << endl;
	return temp;
};

vector<string> parser(string s) {
	vector<string> v;
	if(s.find("with") != string::npos) {
		string s1 = s.substr(0, s.find("with"));
		value_parser(s1, v);
		while(s.find(' ') != string::npos) {
			s = s.substr(s.find(' ') + 1);
			if (s.find('=') < s.find(' ')) {
				v.push_back(s.substr(0, s.find('=')));
				s = s.substr(s.find('=') + 1);
				if(s[0] == '"') {
					s = s.substr(1);
					v.push_back(s.substr(0, s.find('"')));
					v.push_back("||");
				} else {
					string temp = s.substr(0, s.find(' ') - 1);
					value_parser(temp, v);
				}
			} else if(s.find('=') == string::npos) {
				value_parser(s, v);
			}
		}
	} else {
		value_parser(s, v);
	}
	//for(int i = 0; i < v.size(); i++)
	//	cout << v[i] << endl;
	return v;
};

void value_parser(string temp, vector<string>& v) {
	replace_coma(temp);
	replace_bracket(temp);
	replace_felesh(temp);
	istringstream iss(temp);   
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter(v));
	v.push_back("||");
};

void replace_coma (string& s) {
  replace( s.begin(), s.end(), ',', ' '); // replace all ',' to ' '
};

void replace_bracket (string& s) {
  replace( s.begin(), s.end(), '[', ' '); // replace all ',' to ' '
  replace( s.begin(), s.end(), ']', ' '); // replace all ',' to ' '
};

void replace_felesh (string& s) {
  replace( s.begin(), s.end(), '<', ' '); // replace all '"' to '\0'
  replace( s.begin(), s.end(), '>', ' '); // replace all '"' to '\0'
};