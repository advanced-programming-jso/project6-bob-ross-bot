#include "bob.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

Bob::Bob(Command* c) {
	width = atoi(((c->get_value("init"))[0]).c_str());
	height = atoi(((c->get_value("init"))[1]).c_str());
	command = c;
	string s = "group root with inherit=default, position=<0,0>";
	Command* c1 = new Command(s);
	Group* g = new Group(c1);
	root = g;
};

void Bob::set_command(Command* c) {
	command = c;
	string cm_type = c->get_command_type();
	if(cm_type == "draw")
		root->shape_builder(c);
	else if(cm_type == "group")
		root->group_builder(c);
	else if(cm_type == "export") {
		if((command->get_value("export"))[0].substr((command->get_value("export"))[0].find('.')+1) == "svg")
			svg_out();
		else
			cout << "sorry! we could just export .svg files yet. :)" << endl;
	} else if(cm_type == "bring")
		root->move_layer(c);
	else if(cm_type == "ungroup")
		root->ungroup(c);
	else if(cm_type == "move") {
		Command* temp = new Command();
		root->find_moving_obj(c,temp);
		if(c->get_value("move").size() > 1)
			root->move_obj(c, temp);
		else {
			root->shape_builder(temp);
		}
	} else if(cm_type == "list") {
		vector<Layer*> list;
		if(c->get_value("list").size() > 0)
			root->list(c);
		else 
			list = root->get_layers();
			for(int j = 0; j < list.size(); j++) {
				cout << list[j]->get_name() << endl;
			}
	}
};

void Bob::svg_out() {
	string exp;
	exp = "<!DOCTYPE svg PUBLIC \"..//WC//DTD SVG 1.1//EN\" \n \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n<svg height=\"" + int_to_str(height) + "\" width=\"" + int_to_str(width) + "\" version=\"1.1\"\n\txmlns=\"http://www.w3.org/2000/svg\">\n";
	exp += root->export_svg();
	exp += "</svg>";
	ofstream out((command->get_value("export"))[0].c_str());
		out << exp << endl;
    out.close();
};