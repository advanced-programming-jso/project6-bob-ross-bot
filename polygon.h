#include "shape.h"

class Polygon : public Shape {
public:
	Polygon(Command* c);
	void make_shape();
	std::string export_svg();
private:
	Command* command;
};